from typing import List

import pyqtgraph as pg

from .manager import BarManager
from .base import AXIS_WIDTH, NORMAL_FONT


class DatetimeAxis(pg.AxisItem):
    """
    日期时间型坐标轴
    注：AxisItem显示轴值、刻度和标签。最常用于PlotItem。
    """

    def __init__(self, manager: BarManager, *args, **kwargs):
        """"""
        super().__init__(*args, **kwargs)

        # K线管理器
        # 接受传入的K线管理器
        self._manager: BarManager = manager

        self.setPen(width=AXIS_WIDTH)
        self.tickFont = NORMAL_FONT

    def tickStrings(self, values: List[int], scale: float, spacing: int):
        """
        Convert original index to datetime string.
        将原始的索引转化为日期时间字符串
        """
        # Show no axis string if spacing smaller than 1
        if spacing < 1:
            return ["" for i in values]

        strings = []

        for ix in values:
            dt = self._manager.get_datetime(ix)

            if not dt:
                s = ""
            elif dt.hour:
                s = dt.strftime("%Y-%m-%d\n%H:%M:%S")
            else:
                s = dt.strftime("%Y-%m-%d")

            strings.append(s)

        return strings
