"""
普吸金交易辅助系统 - 主程序
"""
from pts.task_engine import TaskEngine
from pts.ui import MainWindow, create_qapp

import locale
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

def main():
    """启动PTS"""

    # 创建一个应用程序对象
    qapp = create_qapp()

    # 创建任务引擎
    task_engine = TaskEngine()

    # 创建主窗口并显示
    main_window = MainWindow(task_engine)
    main_window.showMaximized()

    # 执行应用程序，进入消息循环
    qapp.exec()


if __name__ == "__main__":
    main()
