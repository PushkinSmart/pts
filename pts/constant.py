from enum import Enum


class Exchange(Enum):
    SZSE = "SZSE"
    SSE = "SSE"


class Interval(Enum):
    MINUTE = "1m"
    MINUTE5 = "5m"
    MINUTE30 = "30m"

    HOUR = "1h"
    DAILY = "d"
    WEEKLY = "w"


class METHOD(Enum):
    BZ = '标准操作方法'
    JJ = '激进操作方法'
    DX = '短线反弹操作方法'


# ['月线', '周线', '日线', '60分钟', '30分钟', '15分钟', '5分钟', '1分钟']
FREQS = ['日线', '30分钟', '5分钟', '1分钟']

FREQS_WINDOW = {
    '日线': Interval.DAILY,
    '30分钟': Interval.MINUTE30,
    '5分钟': Interval.MINUTE5,
    '1分钟': Interval.MINUTE,
}

INTERVAL_FREQ = {
    'd': '日线',
    '30m': '30分钟',
    '5m': '5分钟',
    '1m': '1分钟'
}

FREQS_EM = {
    '日线': 'daily',
    '30分钟': '30',
    '5分钟': '5',
    '1分钟': '1',
}

FREQS_BS = {
    '日线': 'd',
    '30分钟': '30',
    '5分钟': '5',
    '1分钟': '1',
}

class TaskType(Enum):
    pz = "盘中"
    ph = "盘后"
