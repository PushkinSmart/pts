# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'bxzj_main.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_BxzjMain(object):
    def setupUi(self, BxzjMain):
        BxzjMain.setObjectName("BxzjMain")
        BxzjMain.resize(1146, 659)
        self.verticalLayout = QtWidgets.QVBoxLayout(BxzjMain)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btnRefreshBlock = QtWidgets.QPushButton(BxzjMain)
        self.btnRefreshBlock.setObjectName("btnRefreshBlock")
        self.horizontalLayout.addWidget(self.btnRefreshBlock)
        self.btnRefreshHis = QtWidgets.QPushButton(BxzjMain)
        self.btnRefreshHis.setObjectName("btnRefreshHis")
        self.horizontalLayout.addWidget(self.btnRefreshHis)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.tabWidget = QtWidgets.QTabWidget(BxzjMain)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.tab)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.twZchybk = QtWidgets.QTableWidget(self.tab)
        self.twZchybk.setObjectName("twZchybk")
        self.twZchybk.setColumnCount(0)
        self.twZchybk.setRowCount(0)
        self.verticalLayout_2.addWidget(self.twZchybk)
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.tab_2)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.twZcgnbk = QtWidgets.QTableWidget(self.tab_2)
        self.twZcgnbk.setObjectName("twZcgnbk")
        self.twZcgnbk.setColumnCount(0)
        self.twZcgnbk.setRowCount(0)
        self.verticalLayout_3.addWidget(self.twZcgnbk)
        self.tabWidget.addTab(self.tab_2, "")
        self.tab_5 = QtWidgets.QWidget()
        self.tab_5.setObjectName("tab_5")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.tab_5)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.twBxls = QtWidgets.QTableWidget(self.tab_5)
        self.twBxls.setObjectName("twBxls")
        self.twBxls.setColumnCount(0)
        self.twBxls.setRowCount(0)
        self.verticalLayout_6.addWidget(self.twBxls)
        self.tabWidget.addTab(self.tab_5, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.tab_3)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.twHgtls = QtWidgets.QTableWidget(self.tab_3)
        self.twHgtls.setObjectName("twHgtls")
        self.twHgtls.setColumnCount(0)
        self.twHgtls.setRowCount(0)
        self.verticalLayout_4.addWidget(self.twHgtls)
        self.tabWidget.addTab(self.tab_3, "")
        self.tab_4 = QtWidgets.QWidget()
        self.tab_4.setObjectName("tab_4")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.tab_4)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.twSgtls = QtWidgets.QTableWidget(self.tab_4)
        self.twSgtls.setObjectName("twSgtls")
        self.twSgtls.setColumnCount(0)
        self.twSgtls.setRowCount(0)
        self.verticalLayout_5.addWidget(self.twSgtls)
        self.tabWidget.addTab(self.tab_4, "")
        self.verticalLayout.addWidget(self.tabWidget)
        self.label = QtWidgets.QLabel(BxzjMain)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.label_2 = QtWidgets.QLabel(BxzjMain)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)

        self.retranslateUi(BxzjMain)
        self.tabWidget.setCurrentIndex(2)
        QtCore.QMetaObject.connectSlotsByName(BxzjMain)

    def retranslateUi(self, BxzjMain):
        _translate = QtCore.QCoreApplication.translate
        BxzjMain.setWindowTitle(_translate("BxzjMain", "Form"))
        self.btnRefreshBlock.setText(_translate("BxzjMain", "刷新增持板块"))
        self.btnRefreshHis.setText(_translate("BxzjMain", "刷新历史"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("BxzjMain", "增持行业板块"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("BxzjMain", "增持概念板块"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_5), _translate("BxzjMain", "北向历史"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("BxzjMain", "沪股通历史"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), _translate("BxzjMain", "深股通历史"))
        self.label.setText(_translate("BxzjMain", "本页显示北向资金情况，数据截止到前一次收盘。所以，在盘中刷新数据没有意义，除非昨天忘记了刷新。该类数据在服务器上生成较晚，收盘后也要晚些时候才能下载到。"))
        self.label_2.setText(_translate("BxzjMain", "“刷新增持板块”功能只偶尔能执行成功（单步跟踪时倒是每次都成功），不知在别人电脑上执行效果如何。如果在您的电脑上也执行不了，可以去掉该功能。我主要用“刷新历史”功能。"))
