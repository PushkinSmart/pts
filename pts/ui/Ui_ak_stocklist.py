# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ak_stocklist.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_AkStockList(object):
    def setupUi(self, AkStockList):
        AkStockList.setObjectName("AkStockList")
        AkStockList.resize(1062, 587)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(AkStockList)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.splitter = QtWidgets.QSplitter(AkStockList)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.groupBox_3 = QtWidgets.QGroupBox(self.splitter)
        self.groupBox_3.setTitle("")
        self.groupBox_3.setObjectName("groupBox_3")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.groupBox_3)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox_2 = QtWidgets.QGroupBox(self.groupBox_3)
        self.groupBox_2.setTitle("")
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.btnRefresh = QtWidgets.QPushButton(self.groupBox_2)
        self.btnRefresh.setStatusTip("")
        self.btnRefresh.setObjectName("btnRefresh")
        self.horizontalLayout_4.addWidget(self.btnRefresh)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem)
        self.edtSearch = QtWidgets.QLineEdit(self.groupBox_2)
        self.edtSearch.setMinimumSize(QtCore.QSize(160, 0))
        self.edtSearch.setMaximumSize(QtCore.QSize(160, 16777215))
        self.edtSearch.setObjectName("edtSearch")
        self.horizontalLayout_4.addWidget(self.edtSearch)
        self.btnSearch = QtWidgets.QPushButton(self.groupBox_2)
        self.btnSearch.setObjectName("btnSearch")
        self.horizontalLayout_4.addWidget(self.btnSearch)
        self.horizontalLayout_3.addLayout(self.horizontalLayout_4)
        self.verticalLayout.addWidget(self.groupBox_2)
        self.label = QtWidgets.QLabel(self.groupBox_3)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.twStockList = QtWidgets.QTableWidget(self.groupBox_3)
        self.twStockList.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.twStockList.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.twStockList.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.twStockList.setObjectName("twStockList")
        self.twStockList.setColumnCount(0)
        self.twStockList.setRowCount(0)
        self.verticalLayout.addWidget(self.twStockList)
        self.label_4 = QtWidgets.QLabel(self.groupBox_3)
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName("label_4")
        self.verticalLayout.addWidget(self.label_4)
        self.groupBox = QtWidgets.QGroupBox(self.splitter)
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.vLayoutHistory = QtWidgets.QVBoxLayout()
        self.vLayoutHistory.setObjectName("vLayoutHistory")
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.vLayoutHistory.addWidget(self.label_2)
        self.twFundFlowHistory = QtWidgets.QTableWidget(self.groupBox)
        self.twFundFlowHistory.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.twFundFlowHistory.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.twFundFlowHistory.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.twFundFlowHistory.setObjectName("twFundFlowHistory")
        self.twFundFlowHistory.setColumnCount(0)
        self.twFundFlowHistory.setRowCount(0)
        self.vLayoutHistory.addWidget(self.twFundFlowHistory)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_3 = QtWidgets.QLabel(self.groupBox)
        self.label_3.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout.addWidget(self.label_3)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.btnRefreshStockHis = QtWidgets.QPushButton(self.groupBox)
        self.btnRefreshStockHis.setMinimumSize(QtCore.QSize(120, 0))
        self.btnRefreshStockHis.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.btnRefreshStockHis.setObjectName("btnRefreshStockHis")
        self.horizontalLayout.addWidget(self.btnRefreshStockHis)
        self.btnKLine = QtWidgets.QPushButton(self.groupBox)
        self.btnKLine.setMinimumSize(QtCore.QSize(120, 0))
        self.btnKLine.setObjectName("btnKLine")
        self.horizontalLayout.addWidget(self.btnKLine)
        self.vLayoutHistory.addLayout(self.horizontalLayout)
        self.verticalLayout_5.addLayout(self.vLayoutHistory)
        self.verticalLayout_2.addWidget(self.splitter)
        self.label_5 = QtWidgets.QLabel(AkStockList)
        self.label_5.setMaximumSize(QtCore.QSize(16777215, 22))
        self.label_5.setObjectName("label_5")
        self.verticalLayout_2.addWidget(self.label_5)
        self.label_6 = QtWidgets.QLabel(AkStockList)
        self.label_6.setMaximumSize(QtCore.QSize(16777215, 22))
        self.label_6.setObjectName("label_6")
        self.verticalLayout_2.addWidget(self.label_6)

        self.retranslateUi(AkStockList)
        QtCore.QMetaObject.connectSlotsByName(AkStockList)

    def retranslateUi(self, AkStockList):
        _translate = QtCore.QCoreApplication.translate
        AkStockList.setWindowTitle(_translate("AkStockList", "Form"))
        self.btnRefresh.setText(_translate("AkStockList", "刷新"))
        self.btnSearch.setText(_translate("AkStockList", "查找个股"))
        self.label.setText(_translate("AkStockList", "个股资金流入排名 - 当日"))
        self.label_4.setText(_translate("AkStockList", "单击某支个股在右侧显示其详细的资金流向信息。双击某支个股切换到它的缠论分析界面。"))
        self.label_2.setText(_translate("AkStockList", "历史资金列表"))
        self.label_3.setText(_translate("AkStockList", "个股资金流向图"))
        self.btnRefreshStockHis.setText(_translate("AkStockList", "刷新历史资金列表"))
        self.btnKLine.setText(_translate("AkStockList", "K线图(缠论分析)"))
        self.label_5.setText(_translate("AkStockList", "本页显示个股的实时资金流。可以盘中实时刷新，但由于股票数量较多，建议降低刷新频率。"))
        self.label_6.setText(_translate("AkStockList", "按“K线图(缠论分析)”按钮，打开新的窗口显示当前个股的K线图表，在其中显示该个股的行情数据及缠论分析结果。"))
