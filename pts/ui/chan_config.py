import sys
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QIcon

from .Ui_chan_config import Ui_ChanConfig
from ..setting import CHANSETTINGS, CHANSETTING_FILENAME
from ..utility import save_json


class ChanConfig(QDialog, Ui_ChanConfig):
    def __init__(self):
        super(ChanConfig, self).__init__()
        self.setupUi(self)

        self.initUi()

    def initUi(self):
        '''初始化界面'''
        # 设置窗口的图标
        self.setWindowIcon(QIcon(r'images\YuLan.png'))

        # 缠论类型。0 - 线中枢、1 - 笔中枢 - 1、2 - 笔中枢维持 - 2、3 - 笔中枢 - 3、4 - 笔中枢维持 - 4
        if CHANSETTINGS["ChanKind"] == 1:
            self.radioChanKind_1.setChecked(True)
        elif CHANSETTINGS["ChanKind"] == 2:
            self.radioChanKind_2.setChecked(True)
        elif CHANSETTINGS["ChanKind"] == 3:
            self.radioChanKind_3.setChecked(True)
        elif CHANSETTINGS["ChanKind"] == 4:
            self.radioChanKind_4.setChecked(True)
        else:
            self.radioChanKind_0.setChecked(True)

        # 包含关系的处理方法。1 - 经典理论法；2 - 逐K线分析法
        if CHANSETTINGS["TypeBaoHan"] == 1:
            self.radioBaoHan1.setChecked(True)
        else:
            self.radioBaoHan2.setChecked(True)

        # 笔划分：划分前处理包含关系。1 - 处理；0 - 不处理
        if CHANSETTINGS["TypeBi1"] == 1:
            self.checkBi1.setChecked(True)
        # 笔划分：进行笔的修正。1 - 修正；0 - 不修正
        if CHANSETTINGS["TypeBi2"] == 1:
            self.checkBi2.setChecked(True)
        # 笔划分：允许次高(低)点成笔。1 - 允许；0 - 不允许
        if CHANSETTINGS["TypeBi3"] == 1:
            self.checkBi3.setChecked(True)
        # 笔划分：允许缺口成笔。1 - 允许；0 - 不允许
        if CHANSETTINGS["TypeBi4"] == 1:
            self.checkBi4.setChecked(True)
        # 笔划分：允许顶底K线重叠。1 - 允许；0 - 不允许
        if CHANSETTINGS["TypeBi5"] == 1:
            self.checkBi5.setChecked(True)

        # 允许带缺口的笔升为线段。1 - 允许；0 - 不允许
        if CHANSETTINGS["TypeQuekou"] == 1:
            self.checkQuekou.setChecked(True)

        # 对结果进行标准化处理。1 - 处理；0 - 不处理
        if CHANSETTINGS["TypeBiaoZhunHua"] == 1:
            self.checkBiaoZhunHua.setChecked(True)

        # 中枢划分方法。1 - 限制延伸，9段中枢结束；2 - 不限制延伸，中枢可超过9段
        if CHANSETTINGS["TypeZhongBreak"] == 1:
            self.radioZh1.setChecked(True)
        else:
            self.radioZh2.setChecked(True)

    def SaveSetting(self):
        if self.radioChanKind_1.isChecked():
            CHANSETTINGS["ChanKind"] = 1
        elif self.radioChanKind_2.isChecked():
            CHANSETTINGS["ChanKind"] = 2
        elif self.radioChanKind_3.isChecked():
            CHANSETTINGS["ChanKind"] = 3
        elif self.radioChanKind_4.isChecked():
            CHANSETTINGS["ChanKind"] = 4
        else:
            CHANSETTINGS["ChanKind"] = 0

        if self.radioBaoHan1.isChecked():
            CHANSETTINGS["TypeBaoHan"] = 1
        else:
            CHANSETTINGS["TypeBaoHan"] = 2

        if self.checkBi1.isChecked():
            CHANSETTINGS["TypeBi1"] = 1
        else:
            CHANSETTINGS["TypeBi1"] = 0
        if self.checkBi2.isChecked():
            CHANSETTINGS["TypeBi2"] = 1
        else:
            CHANSETTINGS["TypeBi2"] = 0
        if self.checkBi3.isChecked():
            CHANSETTINGS["TypeBi3"] = 1
        else:
            CHANSETTINGS["TypeBi3"] = 0
        if self.checkBi4.isChecked():
            CHANSETTINGS["TypeBi4"] = 1
        else:
            CHANSETTINGS["TypeBi4"] = 0
        if self.checkBi5.isChecked():
            CHANSETTINGS["TypeBi5"] = 1
        else:
            CHANSETTINGS["TypeBi5"] = 0

        if self.checkQuekou.isChecked():
            CHANSETTINGS["TypeQuekou"] = 1
        else:
            CHANSETTINGS["TypeQuekou"] = 0

        if self.checkBiaoZhunHua.isChecked():
            CHANSETTINGS["TypeBiaoZhunHua"] = 1
        else:
            CHANSETTINGS["TypeBiaoZhunHua"] = 0

        if self.radioZh1.isChecked():
            CHANSETTINGS["TypeZhongBreak"] = 1
        else:
            CHANSETTINGS["TypeZhongBreak"] = 2

        save_json(CHANSETTING_FILENAME, CHANSETTINGS)

    @pyqtSlot()
    def on_btnOK_clicked(self):
        '''确认'''
        self.SaveSetting()
        self.accept()

    @pyqtSlot()
    def on_btnCancel_clicked(self):
        '''取消'''
        self.reject()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ui = ChanConfig()
    ui.show()
    sys.exit(app.exec_())
