import sys
from PyQt5.QtWidgets import QApplication, QDialog, QTableWidgetItem
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QIcon
from typing import List

from .Ui_mess_browse import Ui_MessBrowse
from pts.task_engine import TaskEngine


class MessBrowse(QDialog, Ui_MessBrowse):
    def __init__(self, task_engine: TaskEngine):
        super(MessBrowse, self).__init__()
        self.setupUi(self)

        # 从主窗口接受到的任务引擎
        self.task_engine: TaskEngine = task_engine

        self.initUi()

    def initUi(self):
        '''初始化界面'''
        # 设置窗口的图标
        self.setWindowIcon(QIcon(r'images\YuLan.png'))

        self.tableWidget.setColumnWidth(0, 700)

        li_mess: List = self.task_engine.get_all_mess()
        self.tableWidget.setRowCount(len(li_mess))

        for i in range(len(li_mess)):
            item = QTableWidgetItem(li_mess[i])
            self.tableWidget.setItem(i, 0, item)

    @pyqtSlot()
    def on_btnExit_clicked(self):
        '''退出'''
        self.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ui = MessBrowse()
    ui.show()
    sys.exit(app.exec_())
