import sys
import os
from PyQt5.QtWidgets import QApplication, QWidget, QMessageBox
from PyQt5.QtCore import pyqtSlot, pyqtSignal
import pandas as pd
import time
from typing import Callable

from pts.task_engine import TaskEngine, Task
from pts.constant import TaskType
from .Ui_real_time import Ui_RealTime
from ..global_functions import displayDataFrame
from ..setting import SETTINGS
from ..web_crawler import stock_market_fund_real_time


class RealTime(QWidget, Ui_RealTime):
    """1-实时资金流 页面"""
    # 当日和5日板块资金流的文件名
    file_real_time = r'Data\real_time.csv'

    # 用于处理自动刷新完成信息的信号
    signal: pyqtSignal = pyqtSignal(str)

    def __init__(self, task_engine: TaskEngine, displayStatusMess: Callable):
        """初始化"""
        super(RealTime, self).__init__()
        self.setupUi(self)

        # 从主窗口接受到的任务引擎
        self.task_engine: TaskEngine = task_engine
        # 主窗口的信息显示函数
        self.displayStatusMess: Callable = displayStatusMess

        # 初始化与本页面相关的任务
        self.initTask()

        # 初始化界面
        self.initUi()

    def initTask(self):
        """初始化与本页面相关的任务"""
        # 盘中任务
        task = Task(TaskType.pz, self.process_task, self.signal.emit, interval=SETTINGS["RW.spPZ_1"])
        self.task_engine.put(task)

        # 为信号关联槽函数
        self.signal.connect(self.display_task)

    def initUi(self):
        """初始化界面"""
        # 在列表中显示数据
        # 注：进入系统时显示上次取得的数据
        self.displayRealTime()

    def process_task(self) -> str:
        """
        任务的执行函数，供任务引擎的守护线程调用
        :return: 任务执行的结果信息
        """
        return self.reloadRealTime()

    def display_task(self, mess: str):
        """
        任务执行结果的显示函数，供任务引擎的守护线程调用
        :param mess: 任务执行的结果信息
        :return: 无
        """
        # 在主窗口的状态栏上显示结果信息
        self.displayStatusMess(mess)
        # 更新页面上的列表
        self.displayRealTime()

    def reloadRealTime(self) -> str:
        # 下载数据并保存文件
        stock_data = stock_market_fund_real_time()
        stock_data.to_csv(self.file_real_time, index=False)

        return '刷新实时资金。'

    def displayRealTime(self):
        """在列表中显示数据"""
        # 如果数据文件不存在，重新下载
        if not os.path.exists(self.file_real_time):
            self.reloadRealTime()

        # 从数据文件中读取数据
        stock_data = pd.read_csv(self.file_real_time)

        # 列表显示
        displayDataFrame(stock_data, self.twRealTime, startCol=0, amountList=[1, 2, 3, 4, 5], orderCol=[1, 2, 3, 4, 5])

        # 显示刷新时间
        t = os.path.getmtime(self.file_real_time)
        timeStruct = time.localtime(t)
        s = time.strftime('%Y-%m-%d %H:%M:%S', timeStruct)
        self.lblTime.setText("刷新时间：" + s)

    @pyqtSlot()
    def on_btnRefresh_clicked(self):
        """刷新"""
        # 重新下载
        self.reloadRealTime()

        # 显示当日和5日板块资金流
        self.displayRealTime()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ui = RealTime()
    ui.show()
    sys.exit(app.exec_())
