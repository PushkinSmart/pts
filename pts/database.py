import os
import sqlite3

def OpenMainDb(dbFileName: str, sqlCreate: list):
    file_name = "data/" + dbFileName
    isExists = False
    if os.path.exists(file_name):
        isExists = True

    conn = sqlite3.connect(file_name)
    if not conn:
        return None

    if isExists:
        return conn

    # 创建数据库表
    c = conn.cursor()
    for sqlStr in sqlCreate:
        c.execute(sqlStr)

    return conn


def OpenHyBlockDb():
    sqlCreate = []
    sqlCreate.append('''CREATE TABLE HyBlock (
        rq DATE,
        mc VARCHAR(20),
        zf REAL,
        zlje REAL,
        zljb REAL,
        cdde REAL,
        cddb REAL,
        dde REAL,
        ddb REAL,
        zde REAL,
        zdb REAL,
        xde REAL,
        xdb REAL,
        PRIMARY KEY (rq, mc)
        )
        ''')

    return OpenMainDb('HyBlock.Db', sqlCreate)


def OpenGnBlockDb():
    sqlCreate = []
    sqlCreate.append('''CREATE TABLE GnBlock (
        rq DATE,
        mc VARCHAR(20),
        zf REAL,
        zlje REAL,
        zljb REAL,
        cdde REAL,
        cddb REAL,
        dde REAL,
        ddb REAL,
        zde REAL,
        zdb REAL,
        xde REAL,
        xdb REAL,
        PRIMARY KEY (rq, mc)
        )
        ''')

    return OpenMainDb('GnBlock.Db', sqlCreate)


def OpenAkStockListDb():
    sqlCreate = []
    sqlCreate.append('''CREATE TABLE AkStockList (
        rq DATE,
        dm VARCHAR(20),
        mc VARCHAR(20),
        zxj REAL,
        zf REAL,
        zlje REAL,
        zljb REAL,
        cdde REAL,
        cddb REAL,
        dde REAL,
        ddb REAL,
        zde REAL,
        zdb REAL,
        xde REAL,
        xdb REAL,
        PRIMARY KEY (rq, dm)
        )
        ''')

    return OpenMainDb('AkStockList.Db', sqlCreate)