"""
Global setting of VN Trader.
"""

from typing import Dict, Any

from pts.utility import load_json

# 系统配置
SETTINGS: Dict[str, Any] = {
    "block.number": 20,
    "CurrentIndex1": 0,
    "CurrentIndex2": 0,
    "CurrentIndex3": 0,
    "DataSource": 1,
    "EveryTime": 1,
    "RW.timeOpen1": "09:30",
    "RW.timeClose1": "11:30",
    "RW.timeOpen2": "13:00",
    "RW.timeClose2": "15:00",
    "RW.spPZ_1": 1,
    "RW.spPZ_2": 0,
    "RW.spPZ_3": 30,
    "RW.spPZ_4": 6,
    "RW.spPZ_5": 5,
    "RW.spPZ_6": 0,
    "RW.timePH_1": "15:05",
    "RW.timePH_2": "15:09",
    "RW.timePH_3": "15:08",
    "RW.timePH_4": "15:07",
    "RW.timePH_5": "15:06",
    "RW.timePH_6": "18:11",
    "RW.datePH_1": "2000-01-01",
    "RW.datePH_2": "2000-01-01",
    "RW.datePH_3": "2000-01-01",
    "RW.datePH_4": "2000-01-01",
    "RW.datePH_5": "2000-01-01",
    "RW.datePH_6": "2000-01-01",
}
# 上述任务编号与功能的对应关系为：
# 1-实时资金流
# 2-大盘资金流
# 3-个股资金流
# 4-行业板块资金流
# 5-概论板块资金流
# 6-北向资金

# Load global setting from json file.
SETTING_FILENAME: str = "pts_setting.json"
SETTINGS.update(load_json(SETTING_FILENAME))

# 缠论配置
CHANSETTINGS: Dict[str, int] = {
    "ChanKind": 0,          # 缠论类型。0 - 线中枢、1 - 笔中枢 - 1、2 - 笔中枢维持 - 2、3 - 笔中枢 - 3、4 - 笔中枢维持 - 4
    "TypeBaoHan": 2,        # 包含关系的处理方法。1 - 经典理论法；2 - 逐K线分析法
    "TypeBi1": 1,           # 笔划分：划分前处理包含关系。1 - 处理；0 - 不处理
    "TypeBi2": 1,           # 笔划分：进行笔的修正。1 - 修正；0 - 不修正
    "TypeBi3": 1,           # 笔划分：允许次高(低)点成笔。1 - 允许；0 - 不允许
    "TypeBi4": 1,           # 笔划分：允许缺口成笔。1 - 允许；0 - 不允许
    "TypeBi5": 1,           # 笔划分：允许顶底K线重叠。1 - 允许；0 - 不允许
    "TypeQuekou": 1,        # 允许带缺口的笔升为线段。1 - 允许；0 - 不允许
    "TypeBiaoZhunHua": 0,   # 对结果进行标准化处理。1 - 处理；0 - 不处理
    "TypeZhongBreak": 2     # 中枢划分方法。1 - 限制延伸，9段中枢结束；2 - 不限制延伸，中枢可超过9段
}

# Load global setting from json file.
CHANSETTING_FILENAME: str = "chan_setting.json"
CHANSETTINGS.update(load_json(CHANSETTING_FILENAME))
