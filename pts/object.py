from dataclasses import dataclass
from datetime import datetime, date
from .constant import Exchange, Interval


@dataclass
class BarData:
    symbol: str
    exchange: Exchange
    datetime: datetime

    interval: Interval = None
    volume: float = 0
    open_interest: float = 0
    open_price: float = 0
    high_price: float = 0
    low_price: float = 0
    close_price: float = 0

    def __post_init__(self):
        self.vt_symbol = f"{self.symbol}.{self.exchange.value}"


@dataclass
class HistoryRequest:
    symbol: str
    exchange: Exchange
    interval: Interval = None
