"""
普吸金交易辅助系统 - 缠论测试程序
"""
import sys
from PyQt5.QtWidgets import QApplication
from pts.ui.candle_chart_dialog import CandleChartDialog


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ui = CandleChartDialog()
    # ui.set_vt_symbol("001217")
    ui.set_vt_symbol("002528")
    ui.showMaximized()
    sys.exit(app.exec_())